import React, {Component} from 'react';
import {createStackNavigator,createAppContainer} from 'react-navigation';

import Login from './Login';
import Cadastro from './Cadastro';
import MediaType from './MediaType';
import tela4 from './Tela4';
import tela5 from './Tela5';
import tela6 from './Tela6'

const MainNavigator = createStackNavigator({

  Tela1: {screen: Login},
  Tela2: {screen: Cadastro},
  Tela3: {screen: MediaType},
  Tela4: {screen: tela4},
  Tela5: {screen: tela5},
  Tela6: {screen: tela6},
 
},
{
  initialRouteName:'Tela1'
});

const AppContainer =  createAppContainer(MainNavigator);

export default class App extends Component{
  render() {
    return <AppContainer/>;
     
  }
}