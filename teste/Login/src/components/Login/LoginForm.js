import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  TouchableOpacity,

} from 'react-native';

export default class LoginView extends Component {

  constructor(props) {
    super(props);
    state = {
      email   : '',
      password: '',
    }
  }
  validate(text,type)
  {
    alph=/^[a-zA-Z]+$/
    if(type=='username')
      {
        if(alph.test(text))
          {
            this.setState({
                nameValdate:true,

             })
          }
        else
          {
            this.setState({
                nameValdate:false
            })
          }
      }
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/color/48/000000/ninja-head.png'}}/>
          <TextInput style={[styles.inputs,
          !this.state.nameValdate? sytle.error:null]}
              placeholder="username"
              keyboardType="email-address"
              underlineColorAndroid='transparent'

              onChangeText={(email) => this.validate({text,"username"})}/>
        </View>

        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/color/48/000000/privacy.png'}}/>
          <TextInput style={[styles.inputs,
          !this.state.passValdate? sytle.error:null]}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
               onChangeText={(email) => this.validate({text,"username"})}/>
        </View>

        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.alertSelector('login')}>
          <Text style={styles.loginText}>Login</Text>
        </TouchableOpacity>

        <TouchableHighlight style={styles.buttonContainer} onPress={() => this.alertSelector('restore_password')}>
            <Text>Forgot your password?</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.buttonContainer} onPress={() => this.alertSelector('register')}>
            <Text>Register</Text>
        </TouchableHighlight>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffb366',
  },
  inputContainer: {
      borderBottomColor: 'rgba(255,255,255,0.3)',
      backgroundColor: 'rgba(255,255,255,0.3)',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: 'rgba(255,255,255,0.3)',
      flex:1,

  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: '#264d73',
  },
  loginText: {
    color: 'white',
  }
});
