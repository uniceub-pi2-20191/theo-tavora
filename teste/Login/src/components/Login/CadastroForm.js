import React, {Component} from 'react';
import {View, StyleSheet,TouchableOpacity,TextInput,Text} from 'react-native';

export default class CadastroForm extends React.Component{
    render(){
        return(
            <View style=(styles.container)>
            <Text>blablabla style=(styles.)</Text>
            <TextInput "nome" style=(styles.) />
            <TextInput "email" style=(styles.) />
            <TextInput "senha" style=(styles.) />
            <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.alertSelector('login')}>
                      <Text style={styles.loginText}>Login</Text>
                    </TouchableOpacity>
            </View>
        );
    }
}
const styles= StyleSheet.create({
        container:{
                backgroundColor:'#ffff',
                alignItems:'center',
                flexGrow:1,
                justifyContent:'center'
            },

});
const styles = StyleSheet.create({
  container: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffb366',
  },
  inputContainer: {
      borderBottomColor: 'rgba(255,255,255,0.3)',
      backgroundColor: 'rgba(255,255,255,0.3)',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: 'rgba(255,255,255,0.3)',
      flex:1,

  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: '#264d73',
  },
  loginText: {
    color: 'white',
  }
});

