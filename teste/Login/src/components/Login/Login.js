import React, {Component} from 'react';
import {View, StyleSheet, Image, Text, KeyboardAvoidingView, App} from 'react-native';
import LoginForm from './LoginForm';

export default class Login extends Component {
    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style = {styles.container}>
                    <View style = {styles.logoContainer}>
                        <Image
                        style = {styles.logo}
                        source = {require('../images/Naruto.png')}
                        />
                    </View>
                    <View style = {styles.formContainer}>
                        <LoginForm/>
                    </View>
            </KeyboardAvoidingView>
            );
    }
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#ffb366',
        flex:1,
    },
    logoContainer:{
        alignItems:'center',
        flexGrow:1,
        justifyContent:'center'
    },
    logo:{
        width:300,
        height:300
    },
    title:{
        color:'#000000',
        marginTop:1,
        width:50,
        textAlign:'center',
        opacity:0.8
    }
});

