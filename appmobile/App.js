import React, { Component } from 'react';
import { AppRegistry, ScrollView, Image, Text } from 'react-native';
export default class Cebolas extends Component {
  render() {
      return (
        <ScrollView>
          <Text style={{fontSize:96}}>UMA CEBOLA</Text>
          <Image source={{uri: "https://mambo.vteximg.com.br/arquivos/ids/188655/131522.jpg?v=635923433297330000", width: 350, height: 350}} />
          <Text style={{fontSize:96}}>DUAS CEBOLAS E MEIA</Text>
          <Image source={{uri: "http://sgpa.com.br/wp-content/uploads/2017/04/os-beneficios-da-cebola1.jpg", width: 350, height: 350}} />
        </ScrollView>
    );
  }
} 